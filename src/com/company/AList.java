package com.company;


import java.util.Arrays;

class AList<E> implements IList<E> {

    private E[] arr;
    private int size;
    private final int capacity = 10;
    private int index;


    public AList() {
        this.arr = (E[]) new Object[capacity];
    }

    public AList(int capacity) {
        this.arr = (E[]) new Object[capacity];
    }

    public AList(E[] array) {
        this.arr = array;
    }

    @Override
    public void clear() {
        arr = (E[]) new Object[capacity];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {
        if (index > size - 1 || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }

        return arr[index];
    }

    @Override
    public boolean add(E e) {
        if (index == arr.length) {
            growArray();
        }
        arr[index] = e;
        index++;
        size++;
        return true;
    }

    private void growArray() {
        E[] newArray = (E[]) new Object[(int) Math.ceil((arr.length + 1) * 1.2)];
        System.arraycopy(arr, 0, newArray, 0, index);
        arr = newArray;
    }

    @Override
    public boolean add(int index, E e) {
        if (index > arr.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        if (index > size - 1 && index < arr.length) {
            add(e);
        }
        E[] newArray = (E[]) new Object[(int) Math.ceil((arr.length + 1) * 1.2)];

        newArray[index] = e;
        System.arraycopy(arr, index, newArray, index + 1, arr.length - index);
        arr = newArray;


        return true;
    }

    @Override
    public E remove(E e) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == e) {
                removeByIndex(i);
            }
        }
        size--;
        return null;
    }

    @Override
    public E removeByIndex(int index) {
        System.arraycopy(arr, index + 1, arr, index, arr.length - 1 - index);
        size--;
        return null;
    }

    @Override
    public boolean contains(E e) {
        for (int i = 0; i < size; i++) {
            if (arr[i] == e)
                return true;
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public E[] toArray() {
        E[] result = (E[]) new Object[index];
        for (int i = 0; i < index; i++) {
            result[i] = arr[i];
        }
        return result;
    }

    @Override
    public IList<E> subList(int fromIdex, int toIndex) {
        int k = 0;
        if (toIndex > index - 1 || toIndex < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        E[] result = (E[]) new Object[(toIndex - fromIdex) + 1];
        for (int i = fromIdex; i <= toIndex; i++) {
            result[k++] = arr[i];
        }
        return new AList(result);
    }

    @Override
    public boolean removeAll(E[] arr) {
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }

        return true;
    }

    @Override
    public boolean retainAll(E[] arr) {
        E[] result = (E[]) new Object[arr.length];
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    result[k++] = arr[i];
                }
            }
        }
        arr = result;
        return false;
    }
}
