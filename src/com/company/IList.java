package com.company;


    public interface IList<E> {
        void clear();
        int size();
        E get(int index);
        boolean add(E number);
        boolean add(int index, E number);
        E remove(E number);
        E removeByIndex(int index);
        boolean contains(E number);
        void print();
        E[] toArray();
        IList<E>  subList(int fromIndex, int toIndex);
        boolean removeAll(E[] arr);
        boolean retainAll(E[] arr);
    }
